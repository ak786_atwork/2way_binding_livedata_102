package com.example.anil.a2way_binding_livedata_102;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;
import android.view.View;

public class UserModel extends ViewModel {

    MutableLiveData<String> nameLiveData = new MutableLiveData<>();

    public String name = "anil";

    public MutableLiveData<String> getNameLiveData() {
        Log.d("check",nameLiveData.getValue() + " setNameLiveData called");
        return nameLiveData;
    }

    public void setNameLiveData(MutableLiveData<String> nameLiveData) {
        Log.d("check",nameLiveData.getValue() + " getNameLiveData called");
        this.nameLiveData = nameLiveData;
    }

    public void onClick(View view)
    {
//        nameLiveData.postValue("changing model");
        Log.d("check",nameLiveData.getValue());
        nameLiveData.postValue("jjjjjjjjjj");
       // name = " ak ";
    }


}
