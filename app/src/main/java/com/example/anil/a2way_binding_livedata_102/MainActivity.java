package com.example.anil.a2way_binding_livedata_102;

//working

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.anil.a2way_binding_livedata_102.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        //get view model
        final UserModel userModel = ViewModelProviders.of(this).get(UserModel.class);
        ActivityMainBinding activityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        activityMainBinding.setViewModel(userModel);

        // must set this with live data
        //To use a LiveData object with your binding class, you need to specify a lifecycle owner to define the scope of the LiveData object.
        activityMainBinding.setLifecycleOwner(this );

        activityMainBinding.executePendingBindings();

        /*userModel.getNameLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
            }
        });*/
    }
}
